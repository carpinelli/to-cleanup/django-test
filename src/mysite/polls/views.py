from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse


def index(request) -> HttpResponse:
    """Return HTTP response for polls index page.
    :return the simplest possible view in Django."""
    return HttpResponse("Hello, World. You're at the polls index.")
